public class Board{
	private Die d1;
	private Die d2;
	private boolean[] closedTiles;
	
	public Board(){
		this.d1 = new Die();
		this.d2 = new Die();
		closedTiles = new boolean[12];
	}
	
	public String toString(){
		String s = "";
		for(int i = 0; i < closedTiles.length; i++){
			if(closedTiles[i] == true){
				s = s+(i+1)+" ";
			}
			else{
				s = s+"X"+" ";
			}
		}
		return s;
	}
	
	public boolean playATurn(){
		d1.roll();
		d2.roll();
		System.out.println(d1);
		System.out.println(d2);
		int sum = d1.getPips()+d2.getPips();
		if(!closedTiles[sum-1]){
			closedTiles[sum-1] = true;
			System.out.println("Closing title"+sum);
			return false;
		}
		else{
			System.out.println("This position is already shut");
			return true;
		}
		
	}
}
import java.util.Scanner;
public class ShutTheBox{
	
	public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		System.out.println("Welcome Player!");
		Board b = new Board();
		boolean gameOver = false;
		
		int player1=0;
		int player2=0;
		while(!gameOver){
			System.out.println("Player 1's turn");
			System.out.println(b);
			if(b.playATurn()){
				System.out.println("Player 2 won!");
				player2++;
				System.out.println("Enter yes to play again");
				
				String rewind = in.next();
				if(rewind.equals("yes")){
					b = new Board();
					continue;
				}
				System.out.println("Player 1 won"+player1+" times and player 2 won"+player2+" times");
				gameOver = true;
			}
			else{
				System.out.println("Player 2's turn");
				System.out.println(b);
				
				if(b.playATurn()){
					System.out.println("Player 1 won!");
					player1++;
					System.out.println("Enter yes to play again");
					System.out.println("");
					String rewind = in.next();
					if(rewind.equals("yes")){
						b = new Board();
						continue;
					}
					System.out.println("Player 1 won"+player1+" times and player 2 won"+player1+" times");
					gameOver = true;
				}
		
	}
}
	}
}